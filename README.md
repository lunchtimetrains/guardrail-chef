guardrail Cookbook
==================
Install the Scriptrock Guardrail Client

Requirements
------------

#### cookbooks
- `apt` - required to install on Debian

Attributes
----------

#### guardrail::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['guardrail']['secretpath']</tt></td>
    <td>String</td>
    <td>The path to the key used to encrypt the data bag</td>
    <td><tt>/etc/chef/edb_keys/guardrail.key</tt></td>
  </tr>
  <tr>
    <td><tt>['guardrail']['organisation']</tt></td>
    <td>String</td>
    <td>The Guardrail Organisation that this node is associated with</td>
    <td><tt>NULL</tt></td>
  </tr>
</table>

Usage
-----
#### guardrail::default

Create an encrypted data bag with the following content and copy the
secret key to /etc/chef/edb_keys/guardrail.key:

```json
{
      "id": "api_keys",
      "api_key": "your api key here"
}
```

Then include `guardrail` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[guardrail]"
  ]
}
```

Contributing
------------
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Matthew Macdonald-Wallace

Released under the Apache2 License
