name             'guardrail'
maintainer       'Lunchtime Trains'
maintainer_email 'hello@lunchtimetrains.co.uk'
license          'Apache2'
description      'Installs/Configures guardrail'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends          'java'
