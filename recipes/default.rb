#
# Cookbook Name:: guardrail
# Recipe:: default
#
# Copyright 2014, Lunch Time Trains
#
# Licensed under the Apache2 License
#

include_recipe 'java'

## we use HTTPS to download the apt packages so
# we need to enable the https transport
package "apt-transport-https" do
    action :install
end

## Add the apt repository
apt_repository "scriptrock-guardrail" do
  uri "https://download.scriptrock.com/apt"
  distribution "ubuntu"
  components ["main"]
  key "https://download.scriptrock.com/apt/scriptrock.gpg.pub"
end

# install the package
package "scriptrock" do
    action :install
end

# the debian package doesn't create the config directory
# so we need to do that here
directory "/etc/scriptrock" do
    action :create
end

guardrail_secret = Chef::EncryptedDataBagItem.load_secret("#{node[:guardrail][:secretpath]}")
guardrail_creds = Chef::EncryptedDataBagItem.load("guardrail", "api_keys", guardrail_secret)

template "/etc/scriptrock/scriptrock.yml" do
    source "scriptrock.yml.erb"
    owner "root"
    group "root"
    mode  0640
    variables({
        :api_key => guardrail_creds['api_key']
    })
end

# set the java options
template "/etc/profile.d/scriptrock.sh" do
    source "scriptrock_java.sh"
    owner "root"
    group "root"
    mode  0640
end

## Before we start the service we need to register with the site
bash "register_scriptrock" do
    not_if "file /etc/scriptrock/license.dat"
    code <<-EOH
scriptrock -r --config-file=/etc/scriptrock/scriptrock.yml
EOH
end

# The scriptrockd init script seems to hang for some reason
# and the service won't start. Let's start it manually instead
bash "start_scriptrock" do
    code <<-EOH
source /etc/profile
/opt/scriptrock/bin/scriptrockd start
EOH
end
